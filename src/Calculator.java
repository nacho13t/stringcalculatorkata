/**
 *
 * @author nacho
 */
class Calculator {
    
    private final String delimiter = ",|\n"; 
    
    public int calculate(String input) throws Exception{
        if(input.isEmpty())return 0;
        String[] numbers = input.split(delimiter);
        if(input.length() == 1) return Integer.parseInt(input);
        return getSum(numbers);
    }
    
    private int getSum(String[] numbers) throws Exception{
        int sum = 0;
        for (String number : numbers) {
            if(Integer.parseInt(number) < 0) throw new Exception("Negative input");
            if(Integer.parseInt(number)<1000)sum += Integer.parseInt(number);
        }
        return sum;
    }
    
}

import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;
import org.testng.annotations.BeforeTest;

/**
 *
 * @author nacho
 */

@Test
public class TestCalculate {
    private Calculator calculator;
    
    @BeforeTest
    public void init(){
        calculator = new Calculator();
    }
    
    public void empty_string_returns_zero() throws Exception{
        assertEquals(calculator.calculate(""), 0);
    }
    
    public void single_value_reply() throws Exception{
        assertEquals(calculator.calculate("1"), 1);
    }
    
    public void sum_of_two_coma_delimited() throws Exception{
        assertEquals(calculator.calculate("1,2"), 3);
    }
    
    public void sum_of_two_newline_delimited() throws Exception{
        assertEquals(calculator.calculate("1\n2"), 3);
    }
    
    public void sum_of_three() throws Exception{
        assertEquals(calculator.calculate("1,2,3"), 6);
    }
    
    @Test(expectedExceptions = Exception.class)
    public void negative_input_returns_exception() throws Exception{
        calculator.calculate("-1");
    }
    
    public void ignore_greate_than_1000() throws Exception{
        assertEquals(calculator.calculate("1000,2,3"), 5);
    }
    
}
